import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoRaiseResumeComponent } from './auto-raise-resume.component';

describe('AutoRaiseResumeComponent', () => {
  let component: AutoRaiseResumeComponent;
  let fixture: ComponentFixture<AutoRaiseResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoRaiseResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoRaiseResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
