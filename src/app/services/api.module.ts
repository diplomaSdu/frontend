import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {AuthService} from './auth.service';
import {ApiService} from './api.service';
import {ApiListService} from './api/api-list.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        HttpClientModule
    ]
})
export class ApiModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [
                {
                    provide: ApiListService,
                    useClass: ApiListService,
                    deps: [
                        AuthService
                    ]
                }
            ]
        }
    }
}
