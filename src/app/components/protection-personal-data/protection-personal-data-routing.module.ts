import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProtectionPersonalDataComponent} from './protection-personal-data/protection-personal-data.component';


const routes: Routes = [
    {
        path: '',
        component: ProtectionPersonalDataComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProtectionPersonalDataRoutingModule { }
