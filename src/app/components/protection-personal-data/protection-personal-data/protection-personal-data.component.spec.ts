import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectionPersonalDataComponent } from './protection-personal-data.component';

describe('ProtectionPersonalDataComponent', () => {
  let component: ProtectionPersonalDataComponent;
  let fixture: ComponentFixture<ProtectionPersonalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectionPersonalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectionPersonalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
