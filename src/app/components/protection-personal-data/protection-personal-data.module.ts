import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ProtectionPersonalDataComponent} from './protection-personal-data/protection-personal-data.component';
import {ProtectionPersonalDataRoutingModule} from './protection-personal-data-routing.module';


@NgModule({
    declarations: [ProtectionPersonalDataComponent],
    imports: [
        CommonModule,
        ProtectionPersonalDataRoutingModule
    ]
})
export class ProtectionPersonalDataModule { }
