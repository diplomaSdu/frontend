import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {
    isStudent = false;
    signInForm: FormGroup;
    submitted = false;
    errorMessage: string;
    destroy = new Subject();

    constructor(
        private formBuilder: FormBuilder,
        private auth: AuthService
        ) {}

    ngOnInit(): void {
        this.initSignInForm();
    }

    initSignInForm(): void {
        this.signInForm = this.formBuilder.group({
            email: [
                '',
                [Validators.required, Validators.pattern(/[^@]+@[^.]+\..+/)],
            ],
            password: [
                '',
                [Validators.required, Validators.minLength(3)]
            ]
        });
    }

    login() {
        console.log(this.signInForm.valid);
        if (this.signInForm.valid) {
            this.auth.login(this.signInForm.value).pipe(takeUntil(this.destroy)).subscribe(res => {
                console.log(res);
            });
        }
    }

    ngOnDestroy(): void {
        this.destroy.next();
        this.destroy.complete();
    }
}
