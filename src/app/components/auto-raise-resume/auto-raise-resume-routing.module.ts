import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AutoRaiseResumeComponent} from './auto-raise-resume/auto-raise-resume.component';


const routes: Routes = [
    {
        path: '',
        component: AutoRaiseResumeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AutoRaiseResumeRoutingModule { }
