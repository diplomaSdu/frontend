import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AutoRaiseResumeComponent} from './auto-raise-resume/auto-raise-resume.component';
import {AutoRaiseResumeRoutingModule} from './auto-raise-resume-routing.module';
import {TarifComponent} from './tarif/tarif.component';


@NgModule({
    declarations: [AutoRaiseResumeComponent, TarifComponent],
    imports: [
        CommonModule,
        AutoRaiseResumeRoutingModule
    ]
})
export class AutoRaiseResumeModule { }
