export interface Entity<T> {
  success: boolean;
  errors: never;
  data: T;
}
