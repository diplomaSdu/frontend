import {Injectable} from '@angular/core';
import {AuthRM} from '../classes';
import {HttpClient} from '@angular/common/http';
import {ApiService} from './api.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private http: HttpClient,
        private apiService: ApiService
    ) { }

    login(data: AuthRM) {
        return this.http.post(`${this.apiService.getApi()}/login/students`, data);
    }
}
