import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './components/main/main.component';


const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'main'
    },
    {
        path: 'main',
        component: MainComponent
    },
    {
        path: 'auth',
        loadChildren: () => import('./components/auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'protect-personal-data',
        loadChildren: () => import('./components/protection-personal-data/protection-personal-data.module')
            .then(m => m.ProtectionPersonalDataModule)
    },
    {
        path: 'auto-raise-resume',
        loadChildren: () => import('./components/auto-raise-resume/auto-raise-resume.module').then(m => m.AutoRaiseResumeModule)
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
