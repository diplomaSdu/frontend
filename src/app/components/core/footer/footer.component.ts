import {Component} from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
    socialLinks = ['assets/icons/instagram.svg', 'assets/icons/facebook.svg', 'assets/icons/vk.svg', 'assets/icons/whatsapp.svg', 'assets/icons/telegram.svg'];
}
